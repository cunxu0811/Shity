﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using QLCVfinal.View;

namespace QLCVfinal.View
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        SqlConnection con = new SqlConnection(@"Data Source=SWEETMOON\LALA;Initial Catalog=QLCV;Integrated Security=True");

        private void ActiveMdiChild_FormClosed(object sender, FormClosedEventArgs e)
        {
            ((sender as Form).Tag as TabPage).Dispose();
        }

        private void tabForms_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((tabControl1.SelectedTab != null) && (tabControl1.SelectedTab.Tag != null))
                (tabControl1.SelectedTab.Tag as Form).Select();
        }

        private void FrmMain_MdiChildActivate(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild == null)
                tabControl1.Visible = false; // If no any child form, hide tabControl
            else
            {
                this.ActiveMdiChild.WindowState = FormWindowState.Maximized; // Child form always maximized

                // If child form is new and no has tabPage, create new tabPage
                if (this.ActiveMdiChild.Tag == null)
                {
                    // Add a tabPage to tabControl with child form caption
                    TabPage tp = new TabPage(this.ActiveMdiChild.Text);
                    tp.Tag = this.ActiveMdiChild;
                    tp.Parent = tabControl1;
                    tabControl1.SelectedTab = tp;

                    this.ActiveMdiChild.Tag = tp;
                    this.ActiveMdiChild.FormClosed += new FormClosedEventHandler(ActiveMdiChild_FormClosed);
                }

                if (!tabControl1.Visible) tabControl1.Visible = true;
            }
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmCVden f = new FrmCVden();
            f.MdiParent = this;
            f.Show();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmThemMoi f = new FrmThemMoi();
            f.Show();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmCVdi f = new FrmCVdi();
            f.MdiParent = this;
            f.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {

        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string username = FrmDangNhap.username;
            con.Open();
            SqlCommand queryChucVu = new SqlCommand(@"select chucVu from QLCV.dbo.tb_acc where userName = '" + username + "'", con);
            queryChucVu.CommandType = CommandType.Text;
            object oRetVal = queryChucVu.ExecuteScalar();
            string chucVu = string.Empty;
            if (oRetVal != null)
            {
                chucVu = oRetVal.ToString();
            }
            if (chucVu == "admin")
            {
                FrmQuanLyTK f = new FrmQuanLyTK();
                f.MdiParent = this;
                f.Show();
            }
            else MessageBox.Show("Bạn không có quyền ADMIN!!!");
        }

        
    }
}
