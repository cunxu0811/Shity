﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Entity;
using QLCVfinal.db;
using Microsoft.Office.Interop.Word;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using System.Data.Common;
using System.IO;

namespace QLCVfinal.View
{
    public partial class FrmCVdi : Form
    {
        public FrmCVdi()
        {
            InitializeComponent();
        }

        SqlConnection con = new SqlConnection(@"Data Source=SWEETMOON\LALA;Initial Catalog=QLCV;Integrated Security=True");
        SqlConnection objConn = new SqlConnection();
        string strSqlConn = @"Data Source=SWEETMOON\LALA;Initial Catalog=db2;Integrated Security=True";
        string strQuery_GetAttachmentById = "select * from QLCV.dbo.tb_CVDi where [idCVdi] = @attachId";

        private void FrmCVdi_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=SWEETMOON\LALA;Initial Catalog=QLCV;Integrated Security=True");
            con.Open();
            objConn.ConnectionString = strSqlConn;
            Model1 db = new Model1();
            db.tb_acc.Load();
            db.tb_CVDen.Load();
            db.tb_CVDi.Load();
            db.tb_doMat.Load();
            db.tb_DVPH.Load();
            db.tb_loaiCV.Load();
            db.tb_nguoiGui.Load();
            db.tb_nguoiKy.Load();
            db.tb_trangThai.Load();
            tb_CVDiBindingSource.DataSource = db.tb_CVDi.Local.ToBindingList();
            tbdoMatBindingSource.DataSource = db.tb_doMat.Local.ToBindingList();
            tbDVPHBindingSource.DataSource = db.tb_DVPH.Local.ToBindingList();
            tbloaiCVBindingSource.DataSource = db.tb_loaiCV.Local.ToBindingList();
            tbnguoiGuiBindingSource.DataSource = db.tb_nguoiGui.Local.ToBindingList();
            tbnguoiKyBindingSource.DataSource = db.tb_nguoiKy.Local.ToBindingList();
            tbtrangThaiBindingSource.DataSource = db.tb_trangThai.Local.ToBindingList();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            FrmThemMoi f = new FrmThemMoi();
            f.Show();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfdMain = new SaveFileDialog();
            SaveAttachment(sfdMain);
        }

        private void SaveAttachment(SaveFileDialog objSfd)
        {
            string strId = gridView1.GetFocusedRowCellValue("idCVdi").ToString();
            if (!string.IsNullOrEmpty(strId))
            {
                SqlCommand sqlCmd = new SqlCommand(strQuery_GetAttachmentById, objConn);
                sqlCmd.Parameters.AddWithValue("@attachId", strId);
                SqlDataAdapter objAdapter = new SqlDataAdapter(sqlCmd);
                System.Data.DataTable objTable = new System.Data.DataTable();
                DataRow objRow;
                objAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                SqlCommandBuilder sqlCmdBuilder = new SqlCommandBuilder(objAdapter);
                objAdapter.Fill(objTable);
                objRow = objTable.Rows[0];

                byte[] objData;
                objData = (byte[])objRow["attachment"];

                if (objSfd.ShowDialog() != DialogResult.Cancel)
                {
                    string strFileToSave = objSfd.FileName;
                    FileStream objFileStream = new FileStream(strFileToSave, FileMode.Create, FileAccess.Write);
                    objFileStream.Write(objData, 0, objData.Length);
                    objFileStream.Close();
                }
            }
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            gridView1.DeleteRow(gridView1.FocusedRowHandle);
        }

    }
}
