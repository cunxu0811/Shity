﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace QLCVfinal.View
{
    public partial class FrmGui : Form
    {
        public FrmGui()
        {
            InitializeComponent();
        }

        SqlConnection con = new SqlConnection(@"Data Source=SWEETMOON\LALA;Initial Catalog=QLCV;Integrated Security=True"); 
        SqlConnection objConn = new SqlConnection();
        string strSqlConn = @"Data Source=SWEETMOON\LALA;Initial Catalog=QLCV;Integrated Security=True";
        string strQuery_AllAttachments_AllFields = "select * from [tb_CVdi]";

        public string tenFile;
        private void butOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            DialogResult dr = of.ShowDialog();
            if (dr == DialogResult.OK)
            {
                textBoxPath.Text = of.FileName;
                tenFile = of.FileName;
            }
        }

        private void FrmGui_Load(object sender, EventArgs e)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select hoTen from QLCV.dbo.tb_acc", con);
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    string name = dr.GetString(0);
                    comboBoxNguoiNhan.Items.Add(name);
                }
            }

            SqlCommand cmdDoMat = new SqlCommand("select doMat from QLCV.dbo.tb_doMat", con);
            using (SqlDataReader dr = cmdDoMat.ExecuteReader())
            {
                while (dr.Read())
                {
                    string name = dr.GetString(0);
                    comboBoxDoMat.Items.Add(name);
                }
            }

            SqlCommand cmdTrangThai = new SqlCommand("select trangThai from QLCV.dbo.tb_trangThai", con);
            using (SqlDataReader dr = cmdTrangThai.ExecuteReader())
            {
                while (dr.Read())
                {
                    string name = dr.GetString(0);
                    comboBoxTrangThai.Items.Add(name);
                }
            }

            SqlCommand cmdDVPH = new SqlCommand("select donViPhatHanh from QLCV.dbo.tb_DVPH", con);
            using (SqlDataReader dr = cmdDVPH.ExecuteReader())
            {
                while (dr.Read())
                {
                    string name = dr.GetString(0);
                    comboBoxDVPH.Items.Add(name);
                }
            }

            SqlCommand cmdLoaiCV = new SqlCommand("select LoaiCongVan from QLCV.dbo.tb_loaiCV", con);
            using (SqlDataReader dr = cmdLoaiCV.ExecuteReader())
            {
                while (dr.Read())
                {
                    string name = dr.GetString(0);
                    comboBoxLoaiCV.Items.Add(name);
                }
            }

            SqlCommand cmdNguoiKy = new SqlCommand("select tenNguoiKy from QLCV.dbo.tb_nguoiKy", con);
            using (SqlDataReader dr = cmdNguoiKy.ExecuteReader())
            {
                while (dr.Read())
                {
                    string name = dr.GetString(0);
                    comboBoxNguoiKy.Items.Add(name);
                }
            }

            objConn.ConnectionString = strSqlConn;
        }

        private void butGui_Click(object sender, EventArgs e)
        {
            if (comboBoxDoMat.SelectedItem == null)
                MessageBox.Show("Hãy nhập đủ các trường!!!");
            else if (comboBoxDVPH.SelectedItem == null) 
                MessageBox.Show("Hãy nhập đủ các trường!!!");
                 else if (comboBoxNguoiKy.SelectedItem == null)
                    MessageBox.Show("Hãy nhập đủ các trường!!!");
                    else if (comboBoxNguoiNhan.SelectedItem == null)
                        MessageBox.Show("Hãy nhập đủ các trường!!!");
                        else if (comboBoxTrangThai.SelectedItem == null)
                            MessageBox.Show("Hãy nhập đủ các trường!!!");
                            else CreateAttachment(tenFile);
            Int32 rowsAffected;
            SqlCommand updateCVden = new SqlCommand(@"INSERT INTO [tb_CVDen] (tenCVden,maCVden,idDoMat,ngayKy,idDonViPhatHanh,idLoaiCongVan,idTrangThai,idNguoiGui,attachment) 
                                                      SELECT tenCVdi,maCVdi,idDoMat,ngayKy,idDonViPhatHanh,idLoaiCongVan,idTrangThai,idNguoiGui,attachment FROM [tb_CVDi]", con);
            updateCVden.CommandType = CommandType.Text;
            rowsAffected = updateCVden.ExecuteNonQuery();
        }

        private void CreateAttachment(string strFile)
        {
            SqlDataAdapter objAdapter = new SqlDataAdapter(strQuery_AllAttachments_AllFields, objConn);
            objAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
            SqlCommandBuilder objCmdBuilder = new SqlCommandBuilder(objAdapter);
            DataTable objTable = new DataTable();
            FileStream objFileStream = new FileStream(strFile, FileMode.Open, FileAccess.Read);
            int intLength = Convert.ToInt32(objFileStream.Length);
            byte[] objData;
            objData = new byte[intLength];
            DataRow objRow;
            string[] strPath = strFile.Split(Convert.ToChar(@"\"));
            objAdapter.Fill(objTable);

            objFileStream.Read(objData, 0, intLength);
            objFileStream.Close();

            objRow = objTable.NewRow();
            objRow["tenCVdi"] = strPath[strPath.Length - 1];
            objRow["attachment"] = objData;
            objRow["maCVdi"] = textBoxMaCVdi.Text;
            objRow["idNguoiKy"] = (int?)comboBoxNguoiKy.SelectedIndex + 1;
            objRow["idDonViPhatHanh"] = (int?)comboBoxDVPH.SelectedIndex + 1;
            objRow["idDoMat"] = (int?)comboBoxDoMat.SelectedIndex + 1;
            objRow["idLoaiCongVan"] = (int?)comboBoxDVPH.SelectedIndex + 1;
            objRow["idTrangThai"] = (int?)comboBoxTrangThai.SelectedIndex + 1;
            objRow["ngayGui"] = DateTime.Now;

            objTable.Rows.Add(objRow);
            objAdapter.Update(objTable);
            
        }
    }
}
