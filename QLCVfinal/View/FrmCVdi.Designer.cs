﻿namespace QLCVfinal.View
{
    partial class FrmCVdi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCVdi));
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.tb_CVDiBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tb_CVDiGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.coltenCVdi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmaCVdi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colngayGui = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colidDoMat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tbdoMatBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colngayKy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colidNguoiKy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tbnguoiKyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colidDonViPhatHanh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tbDVPHBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colidLoaiCongVan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tbloaiCVBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colidTrangThai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tbtrangThaiBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colidNguoiGui = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tbnguoiGuiBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colpath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.repositoryItemLookUpEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_CVDiBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_CVDiGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbdoMatBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbnguoiKyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDVPHBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbloaiCVBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbtrangThaiBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbnguoiGuiBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit5)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(12, 12);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Thêm mới";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(93, 12);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Xem";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.Image")));
            this.simpleButton3.Location = new System.Drawing.Point(174, 12);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 2;
            this.simpleButton3.Text = "Xóa";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // tb_CVDiBindingSource
            // 
            this.tb_CVDiBindingSource.DataSource = typeof(QLCVfinal.db.tb_CVDi);
            // 
            // tb_CVDiGridControl
            // 
            this.tb_CVDiGridControl.DataSource = this.tb_CVDiBindingSource;
            this.tb_CVDiGridControl.Location = new System.Drawing.Point(12, 41);
            this.tb_CVDiGridControl.MainView = this.gridView1;
            this.tb_CVDiGridControl.Name = "tb_CVDiGridControl";
            this.tb_CVDiGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1,
            this.repositoryItemLookUpEdit2,
            this.repositoryItemLookUpEdit3,
            this.repositoryItemLookUpEdit4,
            this.repositoryItemImageEdit1,
            this.repositoryItemLookUpEdit5,
            this.repositoryItemLookUpEdit6,
            this.repositoryItemLookUpEdit7});
            this.tb_CVDiGridControl.Size = new System.Drawing.Size(795, 434);
            this.tb_CVDiGridControl.TabIndex = 5;
            this.tb_CVDiGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.coltenCVdi,
            this.colmaCVdi,
            this.colngayGui,
            this.colidDoMat,
            this.colngayKy,
            this.colidNguoiKy,
            this.colidDonViPhatHanh,
            this.colidLoaiCongVan,
            this.colidTrangThai,
            this.colidNguoiGui,
            this.colpath});
            this.gridView1.GridControl = this.tb_CVDiGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            // 
            // coltenCVdi
            // 
            this.coltenCVdi.Caption = "Tên CV đi";
            this.coltenCVdi.FieldName = "tenCVdi";
            this.coltenCVdi.Name = "coltenCVdi";
            this.coltenCVdi.OptionsColumn.AllowEdit = false;
            this.coltenCVdi.Visible = true;
            this.coltenCVdi.VisibleIndex = 0;
            // 
            // colmaCVdi
            // 
            this.colmaCVdi.Caption = "Mã CV đi";
            this.colmaCVdi.FieldName = "maCVdi";
            this.colmaCVdi.Name = "colmaCVdi";
            this.colmaCVdi.OptionsColumn.AllowEdit = false;
            this.colmaCVdi.Visible = true;
            this.colmaCVdi.VisibleIndex = 1;
            // 
            // colngayGui
            // 
            this.colngayGui.Caption = "Ngày gửi";
            this.colngayGui.FieldName = "ngayGui";
            this.colngayGui.Name = "colngayGui";
            this.colngayGui.OptionsColumn.AllowEdit = false;
            this.colngayGui.Visible = true;
            this.colngayGui.VisibleIndex = 2;
            // 
            // colidDoMat
            // 
            this.colidDoMat.Caption = "Độ mật";
            this.colidDoMat.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.colidDoMat.FieldName = "idDoMat";
            this.colidDoMat.Name = "colidDoMat";
            this.colidDoMat.OptionsColumn.AllowEdit = false;
            this.colidDoMat.Visible = true;
            this.colidDoMat.VisibleIndex = 3;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.DataSource = this.tbdoMatBindingSource;
            this.repositoryItemLookUpEdit1.DisplayMember = "DoMat";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.ValueMember = "idDoMat";
            // 
            // tbdoMatBindingSource
            // 
            this.tbdoMatBindingSource.DataSource = typeof(QLCVfinal.db.tb_doMat);
            // 
            // colngayKy
            // 
            this.colngayKy.Caption = "Ngày ký";
            this.colngayKy.FieldName = "ngayKy";
            this.colngayKy.Name = "colngayKy";
            this.colngayKy.OptionsColumn.AllowEdit = false;
            this.colngayKy.Visible = true;
            this.colngayKy.VisibleIndex = 4;
            // 
            // colidNguoiKy
            // 
            this.colidNguoiKy.Caption = "Người ký";
            this.colidNguoiKy.ColumnEdit = this.repositoryItemLookUpEdit2;
            this.colidNguoiKy.FieldName = "idNguoiKy";
            this.colidNguoiKy.Name = "colidNguoiKy";
            this.colidNguoiKy.OptionsColumn.AllowEdit = false;
            this.colidNguoiKy.Visible = true;
            this.colidNguoiKy.VisibleIndex = 5;
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.AutoHeight = false;
            this.repositoryItemLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit2.DataSource = this.tbnguoiKyBindingSource;
            this.repositoryItemLookUpEdit2.DisplayMember = "tenNguoiKy";
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            this.repositoryItemLookUpEdit2.ValueMember = "idNguoiKy";
            // 
            // tbnguoiKyBindingSource
            // 
            this.tbnguoiKyBindingSource.DataSource = typeof(QLCVfinal.db.tb_nguoiKy);
            // 
            // colidDonViPhatHanh
            // 
            this.colidDonViPhatHanh.Caption = "ĐV phát hành";
            this.colidDonViPhatHanh.ColumnEdit = this.repositoryItemLookUpEdit3;
            this.colidDonViPhatHanh.FieldName = "idDonViPhatHanh";
            this.colidDonViPhatHanh.Name = "colidDonViPhatHanh";
            this.colidDonViPhatHanh.OptionsColumn.AllowEdit = false;
            this.colidDonViPhatHanh.Visible = true;
            this.colidDonViPhatHanh.VisibleIndex = 6;
            // 
            // repositoryItemLookUpEdit3
            // 
            this.repositoryItemLookUpEdit3.AutoHeight = false;
            this.repositoryItemLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit3.DataSource = this.tbDVPHBindingSource;
            this.repositoryItemLookUpEdit3.DisplayMember = "donViPhatHanh";
            this.repositoryItemLookUpEdit3.Name = "repositoryItemLookUpEdit3";
            this.repositoryItemLookUpEdit3.ValueMember = "idDonViPhatHanh";
            // 
            // tbDVPHBindingSource
            // 
            this.tbDVPHBindingSource.DataSource = typeof(QLCVfinal.db.tb_DVPH);
            // 
            // colidLoaiCongVan
            // 
            this.colidLoaiCongVan.Caption = "Loại công văn";
            this.colidLoaiCongVan.ColumnEdit = this.repositoryItemLookUpEdit4;
            this.colidLoaiCongVan.FieldName = "idLoaiCongVan";
            this.colidLoaiCongVan.Name = "colidLoaiCongVan";
            this.colidLoaiCongVan.OptionsColumn.AllowEdit = false;
            this.colidLoaiCongVan.Visible = true;
            this.colidLoaiCongVan.VisibleIndex = 7;
            // 
            // repositoryItemLookUpEdit4
            // 
            this.repositoryItemLookUpEdit4.AutoHeight = false;
            this.repositoryItemLookUpEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit4.DataSource = this.tbloaiCVBindingSource;
            this.repositoryItemLookUpEdit4.DisplayMember = "LoaiCongVan";
            this.repositoryItemLookUpEdit4.Name = "repositoryItemLookUpEdit4";
            this.repositoryItemLookUpEdit4.ValueMember = "idLoaiCongVan";
            // 
            // tbloaiCVBindingSource
            // 
            this.tbloaiCVBindingSource.DataSource = typeof(QLCVfinal.db.tb_loaiCV);
            // 
            // colidTrangThai
            // 
            this.colidTrangThai.Caption = "Trạng thái";
            this.colidTrangThai.ColumnEdit = this.repositoryItemLookUpEdit6;
            this.colidTrangThai.FieldName = "idTrangThai";
            this.colidTrangThai.Name = "colidTrangThai";
            this.colidTrangThai.OptionsColumn.AllowEdit = false;
            this.colidTrangThai.Visible = true;
            this.colidTrangThai.VisibleIndex = 8;
            // 
            // repositoryItemLookUpEdit6
            // 
            this.repositoryItemLookUpEdit6.AutoHeight = false;
            this.repositoryItemLookUpEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit6.DataSource = this.tbtrangThaiBindingSource;
            this.repositoryItemLookUpEdit6.DisplayMember = "trangThai";
            this.repositoryItemLookUpEdit6.Name = "repositoryItemLookUpEdit6";
            this.repositoryItemLookUpEdit6.ValueMember = "idTrangThai";
            // 
            // tbtrangThaiBindingSource
            // 
            this.tbtrangThaiBindingSource.DataSource = typeof(QLCVfinal.db.tb_trangThai);
            // 
            // colidNguoiGui
            // 
            this.colidNguoiGui.Caption = "Người gửi";
            this.colidNguoiGui.ColumnEdit = this.repositoryItemLookUpEdit7;
            this.colidNguoiGui.FieldName = "idNguoiGui";
            this.colidNguoiGui.Name = "colidNguoiGui";
            this.colidNguoiGui.OptionsColumn.AllowEdit = false;
            this.colidNguoiGui.Visible = true;
            this.colidNguoiGui.VisibleIndex = 9;
            // 
            // repositoryItemLookUpEdit7
            // 
            this.repositoryItemLookUpEdit7.AutoHeight = false;
            this.repositoryItemLookUpEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit7.DataSource = this.tbnguoiGuiBindingSource;
            this.repositoryItemLookUpEdit7.DisplayMember = "userName";
            this.repositoryItemLookUpEdit7.Name = "repositoryItemLookUpEdit7";
            this.repositoryItemLookUpEdit7.ValueMember = "idNguoiGui";
            // 
            // tbnguoiGuiBindingSource
            // 
            this.tbnguoiGuiBindingSource.DataSource = typeof(QLCVfinal.db.tb_nguoiGui);
            // 
            // colpath
            // 
            this.colpath.Caption = "Path";
            this.colpath.FieldName = "path";
            this.colpath.Name = "colpath";
            this.colpath.OptionsColumn.AllowEdit = false;
            this.colpath.Visible = true;
            this.colpath.VisibleIndex = 10;
            // 
            // repositoryItemImageEdit1
            // 
            this.repositoryItemImageEdit1.AutoHeight = false;
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            // 
            // repositoryItemLookUpEdit5
            // 
            this.repositoryItemLookUpEdit5.AutoHeight = false;
            this.repositoryItemLookUpEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit5.Name = "repositoryItemLookUpEdit5";
            // 
            // FrmCVdi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(819, 487);
            this.Controls.Add(this.tb_CVDiGridControl);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Name = "FrmCVdi";
            this.Text = "FrmCVdi";
            this.Load += new System.EventHandler(this.FrmCVdi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tb_CVDiBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_CVDiGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbdoMatBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbnguoiKyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDVPHBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbloaiCVBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbtrangThaiBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbnguoiGuiBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private System.Windows.Forms.BindingSource tb_CVDiBindingSource;
        private DevExpress.XtraGrid.GridControl tb_CVDiGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn coltenCVdi;
        private DevExpress.XtraGrid.Columns.GridColumn colmaCVdi;
        private DevExpress.XtraGrid.Columns.GridColumn colngayGui;
        private DevExpress.XtraGrid.Columns.GridColumn colidDoMat;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private System.Windows.Forms.BindingSource tbdoMatBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colngayKy;
        private DevExpress.XtraGrid.Columns.GridColumn colidNguoiKy;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private System.Windows.Forms.BindingSource tbnguoiKyBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colidDonViPhatHanh;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit3;
        private System.Windows.Forms.BindingSource tbDVPHBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colidLoaiCongVan;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit4;
        private System.Windows.Forms.BindingSource tbloaiCVBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colidTrangThai;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit6;
        private System.Windows.Forms.BindingSource tbtrangThaiBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colidNguoiGui;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit7;
        private System.Windows.Forms.BindingSource tbnguoiGuiBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colpath;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit5;

    }
}