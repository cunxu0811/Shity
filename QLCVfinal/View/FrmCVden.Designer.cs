﻿namespace QLCVfinal.View
{
    partial class FrmCVden
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCVden));
            this.tb_CVDenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tb_accBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.butXem = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.tb_CVDenGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.coltenCVden = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colmaCVden = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colngayNhan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colidDoMat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tbdoMatBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colngayKy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colidDonViPhatHanh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tbDVPHBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colidLoaiCongVan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tbloaiCVBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colidTrangThai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tbtrangThaiBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colidNguoiGui = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tbnguoiGuiBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMarqueeProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar();
            this.colpath = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.tb_CVDenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_accBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tb_CVDenGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbdoMatBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDVPHBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbloaiCVBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbtrangThaiBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbnguoiGuiBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMarqueeProgressBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // tb_CVDenBindingSource
            // 
            this.tb_CVDenBindingSource.DataSource = typeof(QLCVfinal.db.tb_CVDen);
            // 
            // tb_accBindingSource
            // 
            this.tb_accBindingSource.DataSource = typeof(QLCVfinal.db.tb_acc);
            // 
            // panelControl1
            // 
            this.panelControl1.AutoSize = true;
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.butXem);
            this.panelControl1.Location = new System.Drawing.Point(13, 13);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(953, 35);
            this.panelControl1.TabIndex = 0;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.Image")));
            this.simpleButton3.Location = new System.Drawing.Point(167, 5);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 2;
            this.simpleButton3.Text = "Xóa";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(86, 5);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Chỉnh sửa";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // butXem
            // 
            this.butXem.Image = ((System.Drawing.Image)(resources.GetObject("butXem.Image")));
            this.butXem.Location = new System.Drawing.Point(5, 5);
            this.butXem.Name = "butXem";
            this.butXem.Size = new System.Drawing.Size(75, 23);
            this.butXem.TabIndex = 0;
            this.butXem.Text = "Xem";
            this.butXem.Click += new System.EventHandler(this.butXem_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.AutoSize = true;
            this.panelControl2.Controls.Add(this.tb_CVDenGridControl);
            this.panelControl2.Location = new System.Drawing.Point(13, 54);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(953, 502);
            this.panelControl2.TabIndex = 1;
            // 
            // tb_CVDenGridControl
            // 
            this.tb_CVDenGridControl.DataSource = this.tb_CVDenBindingSource;
            this.tb_CVDenGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_CVDenGridControl.Location = new System.Drawing.Point(2, 2);
            this.tb_CVDenGridControl.MainView = this.gridView1;
            this.tb_CVDenGridControl.Name = "tb_CVDenGridControl";
            this.tb_CVDenGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1,
            this.repositoryItemMarqueeProgressBar1,
            this.repositoryItemLookUpEdit2,
            this.repositoryItemLookUpEdit3,
            this.repositoryItemLookUpEdit4,
            this.repositoryItemLookUpEdit5});
            this.tb_CVDenGridControl.Size = new System.Drawing.Size(949, 498);
            this.tb_CVDenGridControl.TabIndex = 0;
            this.tb_CVDenGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.coltenCVden,
            this.colmaCVden,
            this.colngayNhan,
            this.colidDoMat,
            this.colngayKy,
            this.colidDonViPhatHanh,
            this.colidLoaiCongVan,
            this.colidTrangThai,
            this.colidNguoiGui,
            this.colpath});
            this.gridView1.GridControl = this.tb_CVDenGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsDetail.DetailMode = DevExpress.XtraGrid.Views.Grid.DetailMode.Default;
            // 
            // coltenCVden
            // 
            this.coltenCVden.Caption = "Tên CV đến";
            this.coltenCVden.FieldName = "tenCVden";
            this.coltenCVden.Name = "coltenCVden";
            this.coltenCVden.OptionsColumn.AllowEdit = false;
            this.coltenCVden.Visible = true;
            this.coltenCVden.VisibleIndex = 0;
            // 
            // colmaCVden
            // 
            this.colmaCVden.Caption = "Mã CV đến";
            this.colmaCVden.FieldName = "maCVden";
            this.colmaCVden.Name = "colmaCVden";
            this.colmaCVden.OptionsColumn.AllowEdit = false;
            this.colmaCVden.Visible = true;
            this.colmaCVden.VisibleIndex = 1;
            // 
            // colngayNhan
            // 
            this.colngayNhan.Caption = "Ngày nhận";
            this.colngayNhan.FieldName = "ngayNhan";
            this.colngayNhan.Name = "colngayNhan";
            this.colngayNhan.OptionsColumn.AllowEdit = false;
            this.colngayNhan.Visible = true;
            this.colngayNhan.VisibleIndex = 2;
            // 
            // colidDoMat
            // 
            this.colidDoMat.Caption = "Độ mật";
            this.colidDoMat.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.colidDoMat.FieldName = "idDoMat";
            this.colidDoMat.Name = "colidDoMat";
            this.colidDoMat.OptionsColumn.AllowEdit = false;
            this.colidDoMat.Visible = true;
            this.colidDoMat.VisibleIndex = 3;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.DataSource = this.tbdoMatBindingSource;
            this.repositoryItemLookUpEdit1.DisplayMember = "DoMat";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.ValueMember = "idDoMat";
            // 
            // tbdoMatBindingSource
            // 
            this.tbdoMatBindingSource.DataSource = typeof(QLCVfinal.db.tb_doMat);
            // 
            // colngayKy
            // 
            this.colngayKy.Caption = "Ngày ký";
            this.colngayKy.FieldName = "ngayKy";
            this.colngayKy.Name = "colngayKy";
            this.colngayKy.OptionsColumn.AllowEdit = false;
            this.colngayKy.Visible = true;
            this.colngayKy.VisibleIndex = 4;
            // 
            // colidDonViPhatHanh
            // 
            this.colidDonViPhatHanh.Caption = "Đơn vị PH";
            this.colidDonViPhatHanh.ColumnEdit = this.repositoryItemLookUpEdit2;
            this.colidDonViPhatHanh.FieldName = "idDonViPhatHanh";
            this.colidDonViPhatHanh.Name = "colidDonViPhatHanh";
            this.colidDonViPhatHanh.OptionsColumn.AllowEdit = false;
            this.colidDonViPhatHanh.Visible = true;
            this.colidDonViPhatHanh.VisibleIndex = 5;
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.AutoHeight = false;
            this.repositoryItemLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit2.DataSource = this.tbDVPHBindingSource;
            this.repositoryItemLookUpEdit2.DisplayMember = "donViPhatHanh";
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            this.repositoryItemLookUpEdit2.ValueMember = "idDonViPhatHanh";
            // 
            // tbDVPHBindingSource
            // 
            this.tbDVPHBindingSource.DataSource = typeof(QLCVfinal.db.tb_DVPH);
            // 
            // colidLoaiCongVan
            // 
            this.colidLoaiCongVan.Caption = "Loại công văn";
            this.colidLoaiCongVan.ColumnEdit = this.repositoryItemLookUpEdit3;
            this.colidLoaiCongVan.FieldName = "idLoaiCongVan";
            this.colidLoaiCongVan.Name = "colidLoaiCongVan";
            this.colidLoaiCongVan.OptionsColumn.AllowEdit = false;
            this.colidLoaiCongVan.Visible = true;
            this.colidLoaiCongVan.VisibleIndex = 6;
            // 
            // repositoryItemLookUpEdit3
            // 
            this.repositoryItemLookUpEdit3.AutoHeight = false;
            this.repositoryItemLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit3.DataSource = this.tbloaiCVBindingSource;
            this.repositoryItemLookUpEdit3.DisplayMember = "LoaiCongVan";
            this.repositoryItemLookUpEdit3.Name = "repositoryItemLookUpEdit3";
            this.repositoryItemLookUpEdit3.ValueMember = "idLoaiCongVan";
            // 
            // tbloaiCVBindingSource
            // 
            this.tbloaiCVBindingSource.DataSource = typeof(QLCVfinal.db.tb_loaiCV);
            // 
            // colidTrangThai
            // 
            this.colidTrangThai.Caption = "Trạng thái";
            this.colidTrangThai.ColumnEdit = this.repositoryItemLookUpEdit4;
            this.colidTrangThai.FieldName = "idTrangThai";
            this.colidTrangThai.Name = "colidTrangThai";
            this.colidTrangThai.OptionsColumn.AllowEdit = false;
            this.colidTrangThai.Visible = true;
            this.colidTrangThai.VisibleIndex = 7;
            // 
            // repositoryItemLookUpEdit4
            // 
            this.repositoryItemLookUpEdit4.AutoHeight = false;
            this.repositoryItemLookUpEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit4.DataSource = this.tbtrangThaiBindingSource;
            this.repositoryItemLookUpEdit4.DisplayMember = "trangThai";
            this.repositoryItemLookUpEdit4.Name = "repositoryItemLookUpEdit4";
            this.repositoryItemLookUpEdit4.ValueMember = "idTrangThai";
            // 
            // tbtrangThaiBindingSource
            // 
            this.tbtrangThaiBindingSource.DataSource = typeof(QLCVfinal.db.tb_trangThai);
            // 
            // colidNguoiGui
            // 
            this.colidNguoiGui.Caption = "Người gửi";
            this.colidNguoiGui.ColumnEdit = this.repositoryItemLookUpEdit5;
            this.colidNguoiGui.FieldName = "idNguoiGui";
            this.colidNguoiGui.Name = "colidNguoiGui";
            this.colidNguoiGui.OptionsColumn.AllowEdit = false;
            this.colidNguoiGui.Visible = true;
            this.colidNguoiGui.VisibleIndex = 8;
            // 
            // repositoryItemLookUpEdit5
            // 
            this.repositoryItemLookUpEdit5.AutoHeight = false;
            this.repositoryItemLookUpEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit5.DataSource = this.tbnguoiGuiBindingSource;
            this.repositoryItemLookUpEdit5.DisplayMember = "userName";
            this.repositoryItemLookUpEdit5.Name = "repositoryItemLookUpEdit5";
            this.repositoryItemLookUpEdit5.ValueMember = "idNguoiGui";
            // 
            // tbnguoiGuiBindingSource
            // 
            this.tbnguoiGuiBindingSource.DataSource = typeof(QLCVfinal.db.tb_nguoiGui);
            // 
            // repositoryItemMarqueeProgressBar1
            // 
            this.repositoryItemMarqueeProgressBar1.Name = "repositoryItemMarqueeProgressBar1";
            // 
            // colpath
            // 
            this.colpath.Caption = "Path";
            this.colpath.FieldName = "path";
            this.colpath.Name = "colpath";
            this.colpath.Visible = true;
            this.colpath.VisibleIndex = 9;
            // 
            // FrmCVden
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(978, 568);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "FrmCVden";
            this.Text = "`";
            this.Load += new System.EventHandler(this.FrmCVden_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tb_CVDenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tb_accBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tb_CVDenGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbdoMatBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDVPHBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbloaiCVBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbtrangThaiBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbnguoiGuiBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMarqueeProgressBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource tb_CVDenBindingSource;
        private System.Windows.Forms.BindingSource tb_accBindingSource;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton butXem;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl tb_CVDenGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn coltenCVden;
        private DevExpress.XtraGrid.Columns.GridColumn colmaCVden;
        private DevExpress.XtraGrid.Columns.GridColumn colngayNhan;
        private DevExpress.XtraGrid.Columns.GridColumn colidDoMat;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private System.Windows.Forms.BindingSource tbdoMatBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colngayKy;
        private DevExpress.XtraGrid.Columns.GridColumn colidDonViPhatHanh;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private System.Windows.Forms.BindingSource tbDVPHBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colidLoaiCongVan;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit3;
        private System.Windows.Forms.BindingSource tbloaiCVBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colidTrangThai;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit4;
        private System.Windows.Forms.BindingSource tbtrangThaiBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colidNguoiGui;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit5;
        private System.Windows.Forms.BindingSource tbnguoiGuiBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar repositoryItemMarqueeProgressBar1;
        private DevExpress.XtraGrid.Columns.GridColumn colpath;


    }
}