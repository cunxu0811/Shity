﻿namespace QLCVfinal.View
{
    partial class FrmGui
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPath = new System.Windows.Forms.TextBox();
            this.butOpenFile = new DevExpress.XtraEditors.SimpleButton();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxNguoiNhan = new System.Windows.Forms.ComboBox();
            this.butGui = new DevExpress.XtraEditors.SimpleButton();
            this.tbaccBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxDoMat = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxMaCVdi = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimeNgayKy = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxDVPH = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxLoaiCV = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxTrangThai = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxNguoiKy = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tbaccBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "File";
            // 
            // textBoxPath
            // 
            this.textBoxPath.Location = new System.Drawing.Point(102, 12);
            this.textBoxPath.Name = "textBoxPath";
            this.textBoxPath.Size = new System.Drawing.Size(244, 20);
            this.textBoxPath.TabIndex = 1;
            // 
            // butOpenFile
            // 
            this.butOpenFile.Location = new System.Drawing.Point(362, 12);
            this.butOpenFile.Name = "butOpenFile";
            this.butOpenFile.Size = new System.Drawing.Size(75, 23);
            this.butOpenFile.TabIndex = 2;
            this.butOpenFile.Text = "Chọn";
            this.butOpenFile.Click += new System.EventHandler(this.butOpenFile_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Người nhận";
            // 
            // comboBoxNguoiNhan
            // 
            this.comboBoxNguoiNhan.FormattingEnabled = true;
            this.comboBoxNguoiNhan.Location = new System.Drawing.Point(102, 41);
            this.comboBoxNguoiNhan.Name = "comboBoxNguoiNhan";
            this.comboBoxNguoiNhan.Size = new System.Drawing.Size(335, 21);
            this.comboBoxNguoiNhan.TabIndex = 3;
            // 
            // butGui
            // 
            this.butGui.Location = new System.Drawing.Point(190, 229);
            this.butGui.Name = "butGui";
            this.butGui.Size = new System.Drawing.Size(75, 23);
            this.butGui.TabIndex = 11;
            this.butGui.Text = "Gửi";
            this.butGui.Click += new System.EventHandler(this.butGui_Click);
            // 
            // tbaccBindingSource
            // 
            this.tbaccBindingSource.DataSource = typeof(QLCVfinal.db.tb_acc);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Độ mật";
            // 
            // comboBoxDoMat
            // 
            this.comboBoxDoMat.FormattingEnabled = true;
            this.comboBoxDoMat.Location = new System.Drawing.Point(102, 68);
            this.comboBoxDoMat.Name = "comboBoxDoMat";
            this.comboBoxDoMat.Size = new System.Drawing.Size(335, 21);
            this.comboBoxDoMat.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Mã CV đi";
            // 
            // textBoxMaCVdi
            // 
            this.textBoxMaCVdi.Location = new System.Drawing.Point(102, 95);
            this.textBoxMaCVdi.Name = "textBoxMaCVdi";
            this.textBoxMaCVdi.Size = new System.Drawing.Size(163, 20);
            this.textBoxMaCVdi.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Ngày ký";
            // 
            // dateTimeNgayKy
            // 
            this.dateTimeNgayKy.Location = new System.Drawing.Point(102, 121);
            this.dateTimeNgayKy.Name = "dateTimeNgayKy";
            this.dateTimeNgayKy.Size = new System.Drawing.Size(335, 20);
            this.dateTimeNgayKy.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Đơn vị PH";
            // 
            // comboBoxDVPH
            // 
            this.comboBoxDVPH.FormattingEnabled = true;
            this.comboBoxDVPH.Location = new System.Drawing.Point(102, 147);
            this.comboBoxDVPH.Name = "comboBoxDVPH";
            this.comboBoxDVPH.Size = new System.Drawing.Size(335, 21);
            this.comboBoxDVPH.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Loại công văn";
            // 
            // comboBoxLoaiCV
            // 
            this.comboBoxLoaiCV.FormattingEnabled = true;
            this.comboBoxLoaiCV.Location = new System.Drawing.Point(102, 175);
            this.comboBoxLoaiCV.Name = "comboBoxLoaiCV";
            this.comboBoxLoaiCV.Size = new System.Drawing.Size(335, 21);
            this.comboBoxLoaiCV.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(271, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Trạng thái";
            // 
            // comboBoxTrangThai
            // 
            this.comboBoxTrangThai.FormattingEnabled = true;
            this.comboBoxTrangThai.Location = new System.Drawing.Point(332, 94);
            this.comboBoxTrangThai.Name = "comboBoxTrangThai";
            this.comboBoxTrangThai.Size = new System.Drawing.Size(105, 21);
            this.comboBoxTrangThai.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 205);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Người ký";
            // 
            // comboBoxNguoiKy
            // 
            this.comboBoxNguoiKy.FormattingEnabled = true;
            this.comboBoxNguoiKy.Location = new System.Drawing.Point(102, 202);
            this.comboBoxNguoiKy.Name = "comboBoxNguoiKy";
            this.comboBoxNguoiKy.Size = new System.Drawing.Size(335, 21);
            this.comboBoxNguoiKy.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(128, 255);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(218, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Chú ý: Không được để trống bất kỳ mục nào";
            // 
            // FrmGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 278);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.comboBoxNguoiKy);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.comboBoxTrangThai);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBoxLoaiCV);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.comboBoxDVPH);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dateTimeNgayKy);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxMaCVdi);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBoxDoMat);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.butGui);
            this.Controls.Add(this.comboBoxNguoiNhan);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.butOpenFile);
            this.Controls.Add(this.textBoxPath);
            this.Controls.Add(this.label1);
            this.Name = "FrmGui";
            this.Text = "FrmGui";
            this.Load += new System.EventHandler(this.FrmGui_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbaccBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPath;
        private DevExpress.XtraEditors.SimpleButton butOpenFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource tbaccBindingSource;
        private System.Windows.Forms.ComboBox comboBoxNguoiNhan;
        private DevExpress.XtraEditors.SimpleButton butGui;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxDoMat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxMaCVdi;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimeNgayKy;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxDVPH;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxLoaiCV;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxTrangThai;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxNguoiKy;
        private System.Windows.Forms.Label label10;
    }
}