﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Entity;
using QLCVfinal.db;
using Microsoft.Office.Interop.Word;
using System.IO;

namespace QLCVfinal.View
{
    public partial class FrmCVden : Form
    {
        public FrmCVden()
        {
            InitializeComponent();
        }

        Model1 database = new Model1();
        SqlConnection objConn = new SqlConnection(@"Data Source=SWEETMOON\LALA;Initial Catalog=db2;Integrated Security=True");
        string strQuery_GetAttachmentById = "select * from QLCV.dbo.tb_CVDen where [idCVden] = @attachId";

        private void FrmCVden_Load(object sender, EventArgs e)
        {
            string username = FrmDangNhap.username;
            objConn.Open();
            SqlCommand queryChucVu = new SqlCommand(@"select chucVu from QLCV.dbo.tb_acc where userName = '" + username + "'", objConn);
            queryChucVu.CommandType = CommandType.Text;
            object oRetVal = queryChucVu.ExecuteScalar();
            string chucVu = string.Empty;
            if (oRetVal != null)
            {
                chucVu = oRetVal.ToString();
            }
            if (chucVu == "giamdoc")
            {
                database.tb_acc.Load();
                database.tb_CVDen.Load();
                database.tb_CVDi.Load();
                database.tb_doMat.Load();
                database.tb_DVPH.Load();
                database.tb_loaiCV.Load();
                database.tb_nguoiGui.Load();
                database.tb_nguoiKy.Load();
                database.tb_trangThai.Load();
                tb_accBindingSource.DataSource = database.tb_acc.Local.ToBindingList();
                tb_CVDenBindingSource.DataSource = database.tb_CVDen.Local.ToBindingList();
                tbdoMatBindingSource.DataSource = database.tb_doMat.Local.ToBindingList();
                tbDVPHBindingSource.DataSource = database.tb_DVPH.Local.ToBindingList();
                tbloaiCVBindingSource.DataSource = database.tb_loaiCV.Local.ToBindingList();
                tbnguoiGuiBindingSource.DataSource = database.tb_nguoiGui.Local.ToBindingList();
                tbtrangThaiBindingSource.DataSource = database.tb_trangThai.Local.ToBindingList();
            }

            if (chucVu == "truongphong")
            {
                database.tb_acc.Load();
                database.tb_CVDen.Where(p => p.idDoMat <= 4).Load();
                database.tb_CVDi.Where(p => p.idDoMat <= 4).Load();
                database.tb_doMat.Load();
                database.tb_DVPH.Load();
                database.tb_loaiCV.Load();
                database.tb_nguoiGui.Load();
                database.tb_nguoiKy.Load();
                database.tb_trangThai.Load();
                tb_accBindingSource.DataSource = database.tb_acc.Local.ToBindingList();
                tb_CVDenBindingSource.DataSource = database.tb_CVDen.Local.ToBindingList();
                tbdoMatBindingSource.DataSource = database.tb_doMat.Local.ToBindingList();
                tbDVPHBindingSource.DataSource = database.tb_DVPH.Local.ToBindingList();
                tbloaiCVBindingSource.DataSource = database.tb_loaiCV.Local.ToBindingList();
                tbnguoiGuiBindingSource.DataSource = database.tb_nguoiGui.Local.ToBindingList();
                tbtrangThaiBindingSource.DataSource = database.tb_trangThai.Local.ToBindingList();
            }

            if (chucVu == "hocvien")
            {
                database.tb_acc.Load();
                database.tb_CVDen.Where(p => p.idDoMat <= 2).Load();
                database.tb_CVDi.Where(p => p.idDoMat <= 2).Load();
                database.tb_doMat.Load();
                database.tb_DVPH.Load();
                database.tb_loaiCV.Load();
                database.tb_nguoiGui.Load();
                database.tb_nguoiKy.Load();
                database.tb_trangThai.Load();
                tb_accBindingSource.DataSource = database.tb_acc.Local.ToBindingList();
                tb_CVDenBindingSource.DataSource = database.tb_CVDen.Local.ToBindingList();
                tbdoMatBindingSource.DataSource = database.tb_doMat.Local.ToBindingList();
                tbDVPHBindingSource.DataSource = database.tb_DVPH.Local.ToBindingList();
                tbloaiCVBindingSource.DataSource = database.tb_loaiCV.Local.ToBindingList();
                tbnguoiGuiBindingSource.DataSource = database.tb_nguoiGui.Local.ToBindingList();
                tbtrangThaiBindingSource.DataSource = database.tb_trangThai.Local.ToBindingList();
            }

            if (chucVu == "vanthu")
            {
                database.tb_acc.Load();
                database.tb_CVDen.Where(p => p.idDoMat <= 3).Load();
                database.tb_CVDi.Where(p => p.idDoMat <= 3).Load();
                database.tb_doMat.Load();
                database.tb_DVPH.Load();
                database.tb_loaiCV.Load();
                database.tb_nguoiGui.Load();
                database.tb_nguoiKy.Load();
                database.tb_trangThai.Load();
                tb_accBindingSource.DataSource = database.tb_acc.Local.ToBindingList();
                tb_CVDenBindingSource.DataSource = database.tb_CVDen.Local.ToBindingList();
                tbdoMatBindingSource.DataSource = database.tb_doMat.Local.ToBindingList();
                tbDVPHBindingSource.DataSource = database.tb_DVPH.Local.ToBindingList();
                tbloaiCVBindingSource.DataSource = database.tb_loaiCV.Local.ToBindingList();
                tbnguoiGuiBindingSource.DataSource = database.tb_nguoiGui.Local.ToBindingList();
                tbtrangThaiBindingSource.DataSource = database.tb_trangThai.Local.ToBindingList();
            }

            if (chucVu == "guest")
            {
                database.tb_acc.Load();
                database.tb_CVDen.Where(p => p.idDoMat <= 1).Load();
                database.tb_CVDi.Where(p => p.idDoMat <= 1).Load();
                database.tb_doMat.Load();
                database.tb_DVPH.Load();
                database.tb_loaiCV.Load();
                database.tb_nguoiGui.Load();
                database.tb_nguoiKy.Load();
                database.tb_trangThai.Load();
                tb_accBindingSource.DataSource = database.tb_acc.Local.ToBindingList();
                tb_CVDenBindingSource.DataSource = database.tb_CVDen.Local.ToBindingList();
                tbdoMatBindingSource.DataSource = database.tb_doMat.Local.ToBindingList();
                tbDVPHBindingSource.DataSource = database.tb_DVPH.Local.ToBindingList();
                tbloaiCVBindingSource.DataSource = database.tb_loaiCV.Local.ToBindingList();
                tbnguoiGuiBindingSource.DataSource = database.tb_nguoiGui.Local.ToBindingList();
                tbtrangThaiBindingSource.DataSource = database.tb_trangThai.Local.ToBindingList();
            }
        }

        private void butXem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfdMain = new SaveFileDialog();
            SaveAttachment(sfdMain);
        }

        private void SaveAttachment(SaveFileDialog objSfd)
        {
            string strId = gridView1.GetFocusedRowCellValue("idCVden").ToString();
            if (!string.IsNullOrEmpty(strId))
            {
                SqlCommand sqlCmd = new SqlCommand(strQuery_GetAttachmentById, objConn);
                sqlCmd.Parameters.AddWithValue("@attachId", strId);
                SqlDataAdapter objAdapter = new SqlDataAdapter(sqlCmd);
                System.Data.DataTable objTable = new System.Data.DataTable();
                DataRow objRow;
                objAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                SqlCommandBuilder sqlCmdBuilder = new SqlCommandBuilder(objAdapter);
                objAdapter.Fill(objTable);
                objRow = objTable.Rows[0];

                byte[] objData;
                objData = (byte[])objRow["attachment"];

                if (objSfd.ShowDialog() != DialogResult.Cancel)
                {
                    string strFileToSave = objSfd.FileName;
                    FileStream objFileStream = new FileStream(strFileToSave, FileMode.Create, FileAccess.Write);
                    objFileStream.Write(objData, 0, objData.Length);
                    objFileStream.Close();
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfdMain = new SaveFileDialog();
            SaveAttachment(sfdMain);
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            gridView1.DeleteRow(gridView1.FocusedRowHandle);

        }

    }
}
