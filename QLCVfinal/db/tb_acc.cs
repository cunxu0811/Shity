namespace QLCVfinal.db
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tb_acc
    {
        public tb_acc()
        {
            tb_nguoiGui = new HashSet<tb_nguoiGui>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int idTaiKhoan { get; set; }

        [Key]
        [StringLength(50)]
        public string userName { get; set; }

        [Required]
        [StringLength(50)]
        public string passWord { get; set; }

        [Required]
        [StringLength(50)]
        public string hoTen { get; set; }

        [Required]
        [StringLength(50)]
        public string donVi { get; set; }

        [Required]
        [StringLength(50)]
        public string chucVu { get; set; }

        public virtual ICollection<tb_nguoiGui> tb_nguoiGui { get; set; }
    }
}
