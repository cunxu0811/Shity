namespace QLCVfinal.db
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<tb_acc> tb_acc { get; set; }
        public virtual DbSet<tb_CVDen> tb_CVDen { get; set; }
        public virtual DbSet<tb_CVDi> tb_CVDi { get; set; }
        public virtual DbSet<tb_doMat> tb_doMat { get; set; }
        public virtual DbSet<tb_DVPH> tb_DVPH { get; set; }
        public virtual DbSet<tb_loaiCV> tb_loaiCV { get; set; }
        public virtual DbSet<tb_nguoiGui> tb_nguoiGui { get; set; }
        public virtual DbSet<tb_nguoiKy> tb_nguoiKy { get; set; }
        public virtual DbSet<tb_trangThai> tb_trangThai { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<tb_acc>()
                .HasMany(e => e.tb_nguoiGui)
                .WithRequired(e => e.tb_acc)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tb_CVDen>()
                .Property(e => e.idDoMat)
                .HasPrecision(18, 0);

            modelBuilder.Entity<tb_CVDen>()
                .Property(e => e.idTrangThai)
                .HasPrecision(18, 0);

            modelBuilder.Entity<tb_CVDi>()
                .Property(e => e.idDoMat)
                .HasPrecision(18, 0);

            modelBuilder.Entity<tb_CVDi>()
                .Property(e => e.idTrangThai)
                .HasPrecision(18, 0);

            modelBuilder.Entity<tb_doMat>()
                .Property(e => e.idDoMat)
                .HasPrecision(18, 0);

            modelBuilder.Entity<tb_trangThai>()
                .Property(e => e.idTrangThai)
                .HasPrecision(18, 0);
        }
    }
}
