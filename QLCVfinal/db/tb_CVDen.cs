namespace QLCVfinal.db
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tb_CVDen
    {
        [StringLength(50)]
        public string tenCVden { get; set; }

        [Key]
        public int idCVden { get; set; }

        [StringLength(50)]
        public string maCVden { get; set; }

        public DateTime? ngayNhan { get; set; }

        public decimal? idDoMat { get; set; }

        public DateTime? ngayKy { get; set; }

        public int? idDonViPhatHanh { get; set; }

        public int? idLoaiCongVan { get; set; }

        public decimal? idTrangThai { get; set; }

        public int? idNguoiGui { get; set; }

        public string path { get; set; }

        public virtual tb_doMat tb_doMat { get; set; }

        public virtual tb_DVPH tb_DVPH { get; set; }

        public virtual tb_loaiCV tb_loaiCV { get; set; }

        public virtual tb_nguoiGui tb_nguoiGui { get; set; }

        public virtual tb_trangThai tb_trangThai { get; set; }
    }
}
