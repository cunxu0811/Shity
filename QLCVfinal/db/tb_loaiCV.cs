namespace QLCVfinal.db
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tb_loaiCV
    {
        public tb_loaiCV()
        {
            tb_CVDen = new HashSet<tb_CVDen>();
            tb_CVDi = new HashSet<tb_CVDi>();
        }

        [Key]
        public int idLoaiCongVan { get; set; }

        [Required]
        [StringLength(50)]
        public string LoaiCongVan { get; set; }

        public virtual ICollection<tb_CVDen> tb_CVDen { get; set; }

        public virtual ICollection<tb_CVDi> tb_CVDi { get; set; }
    }
}
