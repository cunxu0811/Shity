namespace QLCVfinal.db
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tb_trangThai
    {
        public tb_trangThai()
        {
            tb_CVDen = new HashSet<tb_CVDen>();
            tb_CVDi = new HashSet<tb_CVDi>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal idTrangThai { get; set; }

        [Required]
        [StringLength(50)]
        public string trangThai { get; set; }

        public virtual ICollection<tb_CVDen> tb_CVDen { get; set; }

        public virtual ICollection<tb_CVDi> tb_CVDi { get; set; }
    }
}
