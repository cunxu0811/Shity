namespace QLCVfinal.db
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tb_nguoiKy
    {
        public tb_nguoiKy()
        {
            tb_CVDi = new HashSet<tb_CVDi>();
        }

        [Key]
        public int idNguoiKy { get; set; }

        [StringLength(50)]
        public string tenNguoiKy { get; set; }

        [StringLength(50)]
        public string chucVu { get; set; }

        public virtual ICollection<tb_CVDi> tb_CVDi { get; set; }
    }
}
