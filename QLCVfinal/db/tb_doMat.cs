namespace QLCVfinal.db
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tb_doMat
    {
        public tb_doMat()
        {
            tb_CVDen = new HashSet<tb_CVDen>();
            tb_CVDi = new HashSet<tb_CVDi>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal idDoMat { get; set; }

        [StringLength(50)]
        public string DoMat { get; set; }

        public virtual ICollection<tb_CVDen> tb_CVDen { get; set; }

        public virtual ICollection<tb_CVDi> tb_CVDi { get; set; }
    }
}
