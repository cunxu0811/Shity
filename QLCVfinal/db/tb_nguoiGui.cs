namespace QLCVfinal.db
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tb_nguoiGui
    {
        public tb_nguoiGui()
        {
            tb_CVDen = new HashSet<tb_CVDen>();
            tb_CVDi = new HashSet<tb_CVDi>();
        }

        public int idCV { get; set; }

        [Key]
        public int idNguoiGui { get; set; }

        [Required]
        [StringLength(50)]
        public string userName { get; set; }

        public virtual tb_acc tb_acc { get; set; }

        public virtual ICollection<tb_CVDen> tb_CVDen { get; set; }

        public virtual ICollection<tb_CVDi> tb_CVDi { get; set; }
    }
}
